#pragma once
#include <Windows.h>
#include <vector>
#include "PE/Module.h"
#include "SDK/Misc/InterfaceReg.h"

class Entity;

namespace Utilities
{
	void AttachConsole();
	void DetachConsole();
	std::vector<Module> ListModules();
	uintptr_t FollowJump(uintptr_t address);
	bool InterfaceList(HMODULE module, std::vector<InterfaceReg*>& interface_reg_list);
	std::string LastXOf(std::string input, int number);
	std::string GamePath();
	std::wstring StdToWString(std::string text);
	bool MouseInRegion(int x0, int y0, int x1, int y1);
	Entity* LocalPlayer();
	std::uint8_t* PatternScan(void* module, const char* signature);
}