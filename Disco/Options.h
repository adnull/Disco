#pragma once
#include <iostream>

class Color;

namespace Options
{
	extern int rage_choked_packets;
	extern bool rage_antiaim_enabled;
	extern int rage_antiaim_real_yaw;
	extern int rage_antiaim_real_pitch;
	extern int rage_antiaim_fake_yaw;
	extern bool rage_aimbot_silent;

	extern bool legit_aimbot_enabled;
	extern float legit_aimbot_smooth;

	extern bool visuals_esp_box;
	extern bool visuals_esp_outlined;
	extern bool visuals_esp_health;
	extern bool visuals_esp_armour;
	extern bool visuals_esp_name;
	extern bool visuals_esp_distance;
	extern bool visuals_esp_fill;
	extern bool visuals_esp_enemy_only;
	extern bool visuals_esp_on_death;
	extern bool visuals_esp_snaplines;
	
	extern bool visuals_glow_enabled;

	extern bool visuals_chams_enabled;
	extern int visuals_chams_hand_type;
	extern int visuals_chams_weapon_type;
	extern int visuals_chams_players_type;

	extern bool misc_movement_bhop;
	extern bool misc_movement_autostrafe;

	extern bool misc_cheat_animated_clantag;

	extern Color color_menu_background;
	extern Color color_menu_background_alt;
	extern Color color_menu_backdrop;
	extern Color color_menu_outline_out;
	extern Color color_menu_outline_in;
	extern Color color_menu_hovered;
	extern Color color_menu_selected;
}