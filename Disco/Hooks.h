#pragma once
#include <iostream>
#include <Windows.h>

class VmtHook
{
public:
	VmtHook(void* class_ptr)
	{
		this->class_pointer = reinterpret_cast<uintptr_t**>(class_ptr);

		int table_size = 0;

		while (reinterpret_cast<uintptr_t*>(*this->class_pointer)[table_size])
			table_size++;

		original_pointer = *this->class_pointer;

		new_table_pointer = new uintptr_t[sizeof(uintptr_t) * table_size];
		memcpy(new_table_pointer, original_pointer, sizeof(uintptr_t) * table_size);
	}
	~VmtHook()
	{
		RestoreOldTable();
		delete original_pointer;
		delete new_table_pointer;
	}
	void SwapPointer(size_t index, void* new_function)
	{
		new_table_pointer[index] = reinterpret_cast<uintptr_t>(new_function);
	}
	void ApplyNewTable()
	{
		*class_pointer = new_table_pointer;
	}
	void RestoreOldTable()
	{
		*class_pointer = original_pointer;
	}
	template<typename T>
	T GetOriginal(size_t index)
	{
		return reinterpret_cast<T>(original_pointer[index]);
	}
private:
	uintptr_t** class_pointer = nullptr;
	uintptr_t* original_pointer = nullptr;
	uintptr_t* new_table_pointer = nullptr;
};

class Hooks
{
public:
	Hooks(HMODULE module);
	void Install();
	void Restore();
	VmtHook* client;
	VmtHook* input;
	VmtHook* vgui;
	VmtHook* mdl;
	VmtHook* surface;
	WNDPROC original_wnd_proc;
	HWND window;
	HMODULE module;
};

// Factory definitions
typedef void(__thiscall* PaintTraverseFn)(void*, unsigned int, bool, bool);

inline std::unique_ptr<Hooks> hooks;