#pragma once

class CUserCmd;
class QAngle;

namespace Misc
{
	void Bhop(CUserCmd* cmd);
	void ChokePackets(bool& send_packet);
	void FixMovement(QAngle old_angles, CUserCmd* cmd, float fOldForward, float fOldSidemove);
}