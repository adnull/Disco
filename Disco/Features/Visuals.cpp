#include "Visuals.h"
#include "../Memory.h"
#include "../Interfaces.h"
#include "../Options.h"
#include "../Utilities.h"
#include "../GUI/Render.h"
#include "../SDK/ISurface.h"
#include "../SDK/IVEngineClient.h"
#include "../SDK/Entity.h"
#include "../SDK/Misc/CClientState.h"
#include "../SDK/IClientEntityList.h"
#include "../SDK/IVEngineClient.h"
#include "../SDK/ICollideable.h"
#include "../SDK/IVDebugOverlay.h"
#include "../SDK/Misc/CGlobalVars.h"
#include "../SDK/IClientEntity.h"
#include <string>
#include <sstream>

void Visuals::ChokedPackets()
{
	if (!interfaces->Engine->IsInGame())
		return;

	if (Options::rage_choked_packets == 0)
		return;

	int choke_cnt = static_cast<int>(memory->ClientState->m_choked_commands);

	Render::Float("Choke", choke_cnt, 0, Options::rage_choked_packets, 10, 300);
}

bool Visuals::GetBox(Entity* entity, int& x, int& y, int& w, int& h)
{
	Vector v_origin, min, max, flb, brt, blb, frt, frb, brb, blt, flt;
	float left, top, right, bottom;

	v_origin = entity->VecOrigin();
	min = entity->GetCollideable()->OBBMins() + v_origin;
	max = entity->GetCollideable()->OBBMaxs() + v_origin;

	Vector points[] = {
		Vector(min.x, min.y, min.z),
		Vector(min.x, max.y, min.z),
		Vector(max.x, max.y, min.z),
		Vector(max.x, min.y, min.z),
		Vector(max.x, max.y, max.z),
		Vector(min.x, max.y, max.z),
		Vector(min.x, min.y, max.z),
		Vector(max.x, min.y, max.z)
	};

	if (interfaces->DebugOverlay->ScreenPosition(points[3], flb) || interfaces->DebugOverlay->ScreenPosition(points[5], brt)
		|| interfaces->DebugOverlay->ScreenPosition(points[0], blb) || interfaces->DebugOverlay->ScreenPosition(points[4], frt)
		|| interfaces->DebugOverlay->ScreenPosition(points[2], frb) || interfaces->DebugOverlay->ScreenPosition(points[1], brb)
		|| interfaces->DebugOverlay->ScreenPosition(points[6], blt) || interfaces->DebugOverlay->ScreenPosition(points[7], flt))
		return false;

	Vector arr[] = { flb, brt, blb, frt, frb, brb, blt, flt };

	left = flb.x;
	top = flb.y;
	right = flb.x;
	bottom = flb.y;

	for (int i = 0; i < 8; i++)
	{
		if (left > arr[i].x)
			left = arr[i].x;
		if (bottom < arr[i].y)
			bottom = arr[i].y;
		if (right < arr[i].x)
			right = arr[i].x;
		if (top > arr[i].y)
			top = arr[i].y;
	}

	x = (int)left;
	y = (int)top;
	w = (int)(right - left);
	h = (int)(bottom - top);

	return true;
}

void Visuals::DrawBox(int x, int y, int w, int h)
{
	interfaces->Surface->DrawSetTextFont(Fonts::ESP);

	int x_offset = 0;
	int y_offset = 0;

	if (Options::visuals_esp_box)
	{
		if (Options::visuals_esp_outlined)
		{
			interfaces->Surface->DrawSetColor(Colors::Black);
			interfaces->Surface->DrawOutlinedRect(x, y, x + w, y + h);

			x_offset++;
			y_offset++;
		}

		interfaces->Surface->DrawSetColor(Colors::Red);
		interfaces->Surface->DrawOutlinedRect(x - x_offset, y - y_offset, x + w + x_offset, y + h + y_offset);

		x_offset++;
		y_offset++;

		if (Options::visuals_esp_outlined)
		{
			interfaces->Surface->DrawSetColor(Colors::Black);
			interfaces->Surface->DrawOutlinedRect(x - x_offset, y - y_offset, x + w + x_offset, y + h + y_offset);
		}
	}

	// Draw filled rectangle
	if (Options::visuals_esp_fill)
	{
		interfaces->Surface->DrawSetColor(Color(128, 128, 128, 60));
		interfaces->Surface->DrawFilledRect(x, y, x + w, y + h);
	}
}

void Visuals::DrawPlayerData(Entity* entity, int x, int y, int w, int h)
{
	std::vector<std::string> values;
	Entity* local_player = Utilities::LocalPlayer();

	if (Options::visuals_esp_health)
	{
		std::stringstream fmt;
		fmt << "Health: " << entity->Health();
		values.insert(values.end(), fmt.str());
	}

	if (Options::visuals_esp_armour)
	{
		std::stringstream fmt;
		fmt << "Armor: " << entity->Armour();
		values.insert(values.end(), fmt.str());
	}

	if (Options::visuals_esp_name)
	{
		player_info_t info = entity->GetPlayerInfo();
		std::string name = info.name;

		int tw, th;
		interfaces->Surface->GetTextSize(Fonts::ESP, Utilities::StdToWString(name).c_str(), tw, th);

		if (name == "")
			name = "<no name>";

		Render::queue.push_back(ControlRender{ x + (w / 2) - (tw / 2), y - th - Options::visuals_esp_outlined, 0, 0, RenderType::Text, Colors::White, name, Fonts::ESP });
	}

	if (Options::visuals_esp_distance)
	{
		std::stringstream fmt;
		fmt << "Distance: " << local_player->GetDistance(entity);
		values.insert(values.end(), fmt.str());
	}

	int y_offset = y;

	for (std::string item : values)
	{
		Render::queue.push_back(ControlRender{ x + w + 5 + Options::visuals_esp_outlined, y_offset, 0, 0, RenderType::Text, Colors::White, item, Fonts::ESP });
		y_offset += 15;
	}
}

void Visuals::DrawPlantedC4(Entity* entity)
{
	float bomb_time = entity->BombTimer() - memory->GlobalVars->currenttime;

	// Bomb has already exploded
	if (bomb_time < 0)
		return;

	float bomb_time_length = entity->BombTimerLength();

	int width, height;
	interfaces->Surface->GetScreenSize(width, height);

	// Percent of time left, declared as it is commonly reused
	float time_ratio = bomb_time / bomb_time_length;

	// Background for the bomb timer
	interfaces->Surface->DrawSetColor(Options::color_menu_background);
	interfaces->Surface->DrawFilledRect(width / 3, height / 3, (width / 3) * 2, (height / 3) + 3);

	// Colour bar showing the percentage of time left before explosion
	Color bg_color = Color(255 - (255 * time_ratio), 0 + (255 * time_ratio), 0, 255);

	// Bomb timer percentage line
	interfaces->Surface->DrawSetColor(bg_color);
	interfaces->Surface->DrawFilledRect((width / 3) + 1, (height / 3) + 1, ((width / 3) + ((width / 3) * 1) * time_ratio) - 1, ((height / 3) + 3) - 1);

	// Calculate font size so that we can draw bomb time centered
	int wide, tall;

	std::string bomb_time_text = std::to_string(bomb_time);
	std::wstring str2(bomb_time_text.length(), L' ');

	std::copy(bomb_time_text.begin(), bomb_time_text.end(), str2.begin());

	interfaces->Surface->GetTextSize(Fonts::ESP, str2.c_str(), wide, tall);

	// Render the bomb ESP text
	Render::queue.push_back(ControlRender{ ((width / 3) * 2) - ((width / 3) / 2) - (wide / 2), height / 3, 0, 0, RenderType::Text, Colors::White, std::to_string(bomb_time), Fonts::ESP });
	
	// Draw the actual bomb ESP (possibly separate these into 2 functions?)
	interfaces->Surface->DrawSetTextFont(Fonts::ESP);
	int x, y, w, h;

	if (!GetBox(entity, x, y, w, h))
		return;

	interfaces->Surface->DrawSetColor(Colors::Green);
	interfaces->Surface->DrawOutlinedRect(x, y, x + w, y + h);
}

void Visuals::DrawSnaplines(int x, int y, int w, int h)
{
	if (!Options::visuals_esp_snaplines)
		return;

	int width, height;
	interfaces->Surface->GetScreenSize(width, height);

	interfaces->Surface->DrawSetColor(Colors::Black);
	interfaces->Surface->DrawLine(width / 2, height, x + (w / 2), y + h + Options::visuals_esp_outlined); // Maybe -1 here?
}

void Visuals::Run()
{
	if (!interfaces->Engine->IsInGame())
		return;

	Entity* local_player = Utilities::LocalPlayer();

	if (Options::visuals_esp_on_death && local_player->Health() > 0)
		return;

	// draw based on who is closest (to add)
	for (int i = 1; i < interfaces->ClientEntityList->GetHighestEntityIndex(); i++)
	{
		Entity* entity = reinterpret_cast<Entity*>(interfaces->ClientEntityList->GetClientEntity(i));

		if (!entity)
			continue;

		if (entity == local_player)
			continue;

		if (i <= interfaces->Engine->GetMaxClients())
		{
			IClientEntity* i_entity = reinterpret_cast<IClientEntity*>(entity); // holy fk clean up PLZZZ

			if (entity->Team() == local_player->Team() && Options::visuals_esp_enemy_only)
				continue;

			if (i_entity->GetDormant())
				continue;

			if (entity->Health() < 1)
				continue;

			int x, y, w, h;

			if (!GetBox(entity, x, y, w, h))
				continue;

			DrawBox(x, y, w, h);
			DrawPlayerData(entity, x, y, w, h);
			DrawSnaplines(x, y, w, h);
		}
		else if (entity->IsC4())
		{
			DrawPlantedC4(entity);
		}
	}
}