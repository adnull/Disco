#include "Chams.h"
#include "../Memory.h"
#include "../Interfaces.h"
#include "../Options.h"
#include "../Utilities.h"
#include "../Hooks.h"
#include "../SDK/IMaterialSystem.h"
#include "../SDK/IVModelRender.h"
#include "../SDK/IClientEntityList.h"
#include "../SDK/Misc/KeyValues.h"

Chams::Chams()
{
	const auto kv = KeyValues::FromString("VertexLitGeneric", "$baseTexture black $bumpmap effects/flat_normal $translucent 1 $envmap models/effects/crystal_cube_vertigo_hdr $envmapfresnel 0 $phong 1 $phongexponent 16 $phongboost 2");
	kv->SetString("$phongtint", "[.2 .35 .6]");

	Regular = interfaces->MaterialSystem->CreateMaterial("normal", KeyValues::FromString("VertexLitGeneric", nullptr));
	Flat = interfaces->MaterialSystem->CreateMaterial("flat", KeyValues::FromString("UnlitGeneric", nullptr));
	
	{
		const auto kv = KeyValues::FromString("VertexLitGeneric", "$baseTexture black $bumpmap models/inventory_items/trophy_majors/matte_metal_normal $additive 1 $envmap editor/cube_vertigo $envmapfresnel 1 $normalmapalphaenvmapmask 1 $phong 1 $phongboost 20 $phongexponent 3000 $phongdisablehalflambert 1");
		kv->SetString("$phongfresnelranges", "[.1 .4 1]");
		kv->SetString("$phongtint", "[.8 .9 1]");
		Plastic = interfaces->MaterialSystem->CreateMaterial("plastic", kv);
	}
}

Chams::~Chams()
{
}

void Chams::OverrideMaterial(bool ignoreZ, bool flat, bool wireframe, bool glass, const Color& rgba)
{
	IMaterial* material = nullptr;

	if (flat)
		material = Flat;
	else
		material = Regular;

	material->SetMaterialVarFlag(MATERIAL_VAR_IGNOREZ, ignoreZ);

	if (glass)
	{
		material = Flat;
		material->AlphaModulate(0.45f);
	}
	else
	{
		material->AlphaModulate(rgba.a() / 255.0f);
	}

	material->SetMaterialVarFlag(MATERIAL_VAR_WIREFRAME, wireframe);

	material->ColorModulate(
		rgba.r() / 255.0f,
		rgba.g() / 255.0f,
		rgba.b() / 255.0f
	);

	interfaces->ModelRender->ForcedMaterialOverride(material);
}

void Chams::OnDrawModelExecute(IMatRenderContext* ctx, const DrawModelState_t& state, const ModelRenderInfo_t& info, matrix3x4_t* matrix)
{
	if (!Options::visuals_chams_enabled)
		return;

	static auto o_func = hooks->mdl->GetOriginal<void(__thiscall*)(void*, IMatRenderContext*, const DrawModelState_t&, const ModelRenderInfo_t&, matrix3x4_t*)>(21);

	const auto model = info.pModel;

	bool is_arm = strstr(model->szName, "arms") != nullptr;
	bool is_player = strstr(model->szName, "models/player") != nullptr;
	bool is_sleeve = strstr(model->szName, "sleeve") != nullptr;
	bool is_weapon = strstr(model->szName, "models/weapons/v_") != nullptr;

	/*
	static std::vector<std::string> seen;

	if (std::find(seen.begin(), seen.end(), model->szName) == seen.end())
	{
		std::cout << model->szName << std::endl;
		seen.push_back(model->szName);
	}

	^^ Can find materials easily this way
	*/

	if (is_player && Options::visuals_chams_players_type > 0)
	{
		Entity* local_player = Utilities::LocalPlayer();
		Entity* entity = (Entity*)interfaces->ClientEntityList->GetClientEntity(info.entity_index);

		if (!local_player)
			return;

		if (!entity || entity->GetDormant())
			return;

		if (entity->IsAlive())
		{
			OverrideMaterial(false, Options::visuals_chams_players_type == 2, Options::visuals_chams_players_type == 3, Options::visuals_chams_players_type == 4, Colors::Red);
		}
		else
		{
			std::cout << "Rendering a ragdoll" << std::endl;
			OverrideMaterial(false, Options::visuals_chams_players_type == 2, Options::visuals_chams_players_type == 3, Options::visuals_chams_players_type == 4, Colors::Blue);
		}
		/*
		if (Options::Visuals::Chams::IgnoreZ)
		{
			OverrideMaterial(true, Options::Visuals::Chams::Entity == 2, Options::Visuals::Chams::Entity == 3, false, Options::Colors::Visuals::Chams::PlayerInvis);
			o_func(Interfaces::ModelRender, ctx, state, info, matrix);
			OverrideMaterial(false, Options::Visuals::Chams::Entity == 2, Options::Visuals::Chams::Entity == 3, false, Options::Colors::Visuals::Chams::PlayerVis);
		}
		else
		{*/
	//	}
	}
	else if (is_arm && Options::visuals_chams_hand_type > 0)
	{
		IMaterial* material = interfaces->MaterialSystem->FindMaterial(model->szName, TEXTURE_GROUP_MODEL);

		if (!material)
			return;

		if (Options::visuals_chams_hand_type == 1)
		{
			material->SetMaterialVarFlag(MATERIAL_VAR_NO_DRAW, true);
			interfaces->ModelRender->ForcedMaterialOverride(material);
		}
		/*
		else if (Options::Visuals::Chams::IgnoreZ)
		{
			OverrideMaterial(true, Options::Visuals::Chams::Local == 3, Options::Visuals::Chams::Local == 4, false, Options::Colors::Visuals::Chams::Local);
			o_func(Interfaces::ModelRender, ctx, state, info, matrix);
			OverrideMaterial(false, Options::Visuals::Chams::Local == 3, Options::Visuals::Chams::Local == 4, false, Options::Colors::Visuals::Chams::Local);
		}*/
		else
		{
			OverrideMaterial(false, Options::visuals_chams_hand_type == 3, Options::visuals_chams_hand_type == 4, Options::visuals_chams_hand_type == 5, Colors::Blue);
		}
	}
	else if (is_sleeve && Options::visuals_chams_hand_type == 1)
	{
		auto material = interfaces->MaterialSystem->FindMaterial(model->szName, TEXTURE_GROUP_MODEL);

		if (!material)
			return;

		material->SetMaterialVarFlag(MATERIAL_VAR_NO_DRAW, true);
		interfaces->ModelRender->ForcedMaterialOverride(material);
	}
	else if (is_weapon && Options::visuals_chams_weapon_type > 0)
	{
		if (strstr(model->szName, "arms"))
			return;

		IMaterial* material = interfaces->MaterialSystem->FindMaterial(model->szName, TEXTURE_GROUP_MODEL);

		if (!material)
			return;

		if (Options::visuals_chams_weapon_type == 1)
		{
			material->SetMaterialVarFlag(MATERIAL_VAR_NO_DRAW, true);
			interfaces->ModelRender->ForcedMaterialOverride(material);
		}
		/*
		else if (Options::Visuals::Chams::IgnoreZ)
		{
			OverrideMaterial(
				true,
				Options::Visuals::Chams::Weapon == 3,
				Options::Visuals::Chams::Weapon == 4,
				false,
				Options::Colors::Visuals::Chams::Weapon
			);
			o_func(Interfaces::ModelRender, ctx, state, info, matrix);
			OverrideMaterial(
				false,
				Options::Visuals::Chams::Weapon == 3,
				Options::Visuals::Chams::Weapon == 4,
				false,
				Options::Colors::Visuals::Chams::Weapon
			);
		}*/
		else
		{
			OverrideMaterial(
				false,
				Options::visuals_chams_weapon_type == 3,
				Options::visuals_chams_weapon_type == 4,
				false,
				Colors::Blue
			);
		}
	}
}