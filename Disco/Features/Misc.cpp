#include "Misc.h"
#include "../Options.h"
#include "../Utilities.h"
#include "../Interfaces.h"
#include "../Memory.h"
#include "../SDK/Entity.h"
#include "../SDK/IInputSystem.h"
#include "../SDK/Math/QAngle.h"
#include "../SDK/Math/AngleVectors.h"
#include "../SDK/Misc/CUserCmd.h"
#include "../SDK/Misc/CClientState.h"

void Misc::Bhop(CUserCmd* cmd)
{
	if (!Options::misc_movement_bhop)
		return;

	Entity* local_player = Utilities::LocalPlayer();

	if (!local_player)
		return;

	int flags = local_player->Flags();

	static auto was_last_on_ground{ flags & 1 };

	if (GetAsyncKeyState(VK_SPACE) && !(flags & 1) && !was_last_on_ground)
		cmd->buttons &= ~(1 << 1);

	was_last_on_ground = flags & 1;
}

void Misc::ChokePackets(bool& send_packet)
{
	int choke = 0;
	send_packet = true;

	if (Options::rage_antiaim_enabled)
		choke = 1;

	if (Options::rage_choked_packets > choke)
		choke = Options::rage_choked_packets;

	if (choke > static_cast<int>(memory->ClientState->m_choked_commands))
		send_packet = false;
}

void Misc::FixMovement(QAngle old_angles, CUserCmd* cmd, float fOldForward, float fOldSidemove)
{
	float delta_view;
	float f1;
	float f2;

	if (old_angles.yaw < 0.f)
		f1 = 360.0f + old_angles.yaw;
	else
		f1 = old_angles.yaw;

	if (cmd->viewangles.yaw < 0.0f)
		f2 = 360.0f + cmd->viewangles.yaw;
	else
		f2 = cmd->viewangles.yaw;

	if (f2 < f1)
		delta_view = abs(f2 - f1);
	else
		delta_view = 360.0f - abs(f1 - f2);

	delta_view = 360.0f - delta_view;

	cmd->forwardmove = cos(DEG2RAD(delta_view)) * fOldForward + cos(DEG2RAD(delta_view + 90)) * fOldSidemove;
	cmd->sidemove = sin(DEG2RAD(delta_view)) * fOldForward + sin(DEG2RAD(delta_view + 90)) * fOldSidemove;
}