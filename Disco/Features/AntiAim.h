#pragma once

class CUserCmd;
class QAngle;

namespace AntiAim
{
	void Run(CUserCmd* cmd, bool& send_packet, QAngle old_angles);
}