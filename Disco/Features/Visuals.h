#pragma once

class Entity;

namespace Visuals
{
	void ChokedPackets();
	bool GetBox(Entity* entity, int& x, int& y, int& w, int& h);
	void DrawBox(int x, int y, int w, int h);
	void DrawPlayerData(Entity* entity, int x, int y, int w, int h);
	void DrawPlantedC4(Entity* entity);
	void DrawSnaplines(int x, int y, int w, int h);
	void Run();
}