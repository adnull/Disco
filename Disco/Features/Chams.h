#pragma once
#include "../SDK/IMaterial.h"
#include "../SDK/IMaterialSystem.h"
#include "../SDK/Misc/Color.h"

class Chams
{
public:
	Chams();
	~Chams();
	void OverrideMaterial(bool ignoreZ, bool flat, bool wireframe, bool glass, const Color& rgba);
	void OnDrawModelExecute(IMatRenderContext* ctx, const DrawModelState_t& state, const ModelRenderInfo_t& info, matrix3x4_t* matrix);
	IMaterial* Regular;
	IMaterial* Flat;
	IMaterial* Plastic;
};

inline std::unique_ptr<Chams> chams;