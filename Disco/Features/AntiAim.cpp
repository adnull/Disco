#include "AntiAim.h"
#include "../Options.h"
#include "../Memory.h"
#include "../Utilities.h"
#include "../SDK/Entity.h"
#include "../SDK/Misc/CGlobalVars.h"
#include "../SDK/Math/QAngle.h"
#include "../SDK/Misc/CUserCmd.h"
#include <algorithm>

void AntiAim::Run(CUserCmd* cmd, bool& send_packet, QAngle old_angles)
{
	static float y_yaw = 0.f;

	if (!Options::rage_antiaim_enabled)
		return;

	if (cmd->viewangles.pitch != old_angles.pitch)
		return;

	if (cmd->viewangles.yaw != old_angles.yaw)
		return;

	// add "in use" check here/on ladder/swimming(?)

	// LBY breaker moment

}

/*
Temporarily removed to test LBY breaking

QAngle new_angles = cmd->viewangles;

	switch (Options::rage_antiaim_real_pitch)
	{
	case 1:
		new_angles.pitch = 89.f;
		break;
	case 2:
		new_angles.pitch = -89.f;
		break;
	default:
		break;
	}

	new_angles.pitch = std::clamp(new_angles.pitch, -89.0f, 89.0f);

	float current_time = memory->GlobalVars->ServerTime(cmd);
	float current_index = std::fmod(current_time * 20, 36);

	if (!send_packet)
	{
		if (Options::rage_antiaim_fake_yaw)
		{
			new_angles.yaw += Utilities::LocalPlayer()->MaxDesyncAngle();
			new_angles.yaw = std::clamp(new_angles.yaw, -179.0f, 179.0f); // add inplace clamping
		}

		cmd->viewangles = new_angles;

		// Force LBY update to the sent eye angles
		if (fabsf(cmd->sidemove) < 5.0f)
		{
			if (cmd->buttons & CUserCmd::IN_DUCK)
				cmd->sidemove = cmd->tickCount & 1 ? 3.25f : -3.25f;
			else
				cmd->sidemove = cmd->tickCount & 1 ? 1.1f : -1.1f;
		}
	}
	else
	{
		static float jitter_yaw = 0.0f;

		// send real
		switch (Options::rage_antiaim_real_yaw)
		{
		case 1:
			//backwards
			if ((cmd->viewangles.yaw + 180.f) >= 180.f)
				new_angles.yaw = -180 + ((cmd->viewangles.yaw + 180) - 180);
			else
				new_angles.yaw = cmd->viewangles.yaw + 180;
			break;
		case 2:
			// static
			new_angles.yaw = 90.f;
			break;
		case 3:
			//spin
			if (current_index <= 18)
				new_angles.yaw = 0 - ((18 - current_index) * 10);
			else
				new_angles.yaw = ((current_index - 18) * 10);
			break;
		case 4:
			//jitter
			jitter_yaw += ((rand() % 2) == 0) ? -30.f : 30.f;

			if (jitter_yaw >= 179.f)
				jitter_yaw = -179.f;
			else if (jitter_yaw <= -179.f)
				jitter_yaw = 179.f;

			new_angles.yaw = jitter_yaw;
			break;
		default:
			break;
		}

		new_angles.yaw = std::clamp(new_angles.yaw, -179.0f, 179.0f);

		cmd->viewangles = new_angles;
	}
*/