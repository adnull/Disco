﻿#include "Hooks.h"
#include "Interfaces.h"
#include "Utilities.h"
#include "Options.h"
#include "Features/Misc.h"
#include "Features/AntiAim.h"
#include "Features/Visuals.h"
#include "Features/Chams.h"
#include "GUI/Menu.h"
#include "SDK/ISurface.h"
#include "SDK/IVPanel.h"
#include "SDK/IInputSystem.h"
#include "SDK/IVModelRender.h"
#include "SDK/Misc/CUserCmd.h"
#include "Memory.h"
#include "Netvars.h"
#include "GUI/Render.h"
#include "SDK/Misc/KeyValues.h"
#include <chrono>
#include <thread>
#include <Windows.h>

static LRESULT __stdcall WndProc(HWND window, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if (hooks->original_wnd_proc == nullptr)
		return 0;

	if (msg == WM_KEYDOWN && LOWORD(wParam) == 0x2D
		|| ((msg == WM_LBUTTONDOWN || msg == WM_LBUTTONDBLCLK) && 0x2D == VK_LBUTTON)
		|| ((msg == WM_RBUTTONDOWN || msg == WM_RBUTTONDBLCLK) && 0x2D == VK_RBUTTON)
		|| ((msg == WM_MBUTTONDOWN || msg == WM_MBUTTONDBLCLK) && 0x2D == VK_MBUTTON)
		|| ((msg == WM_XBUTTONDOWN || msg == WM_XBUTTONDBLCLK) && 0x2D == HIWORD(wParam) + 4))
	{
		Menu::draw = !Menu::draw;

		if (!Menu::draw)
			interfaces->InputSystem->ResetInputState();
	}

	if (msg == WM_KEYDOWN && LOWORD(wParam) == 0x24)
	{
		hooks->Restore();
		return 0;
	}

	Menu::Controls(msg, wParam, lParam);

	interfaces->InputSystem->EnableInput(!Menu::draw);

	return CallWindowProcW(hooks->original_wnd_proc, window, msg, wParam, lParam);
}

void __fastcall PaintTraverse(void* pPanels, int edx, unsigned int vguiPanel, bool forceRepaint, bool allowForce)
{
	if (hooks->vgui == nullptr)
		return;

	hooks->vgui->GetOriginal<PaintTraverseFn>(41)(pPanels, vguiPanel, forceRepaint, allowForce);

	static unsigned int focus_overlay_panel = 0;
	static bool found_panel = false;

	if (!found_panel)
	{
		const char* panel_name = interfaces->Panel->GetName(vguiPanel);

		if (!strstr(panel_name, "MatSystemTopPanel"))
			return;

		focus_overlay_panel = vguiPanel;
		found_panel = true;
	}

	if (focus_overlay_panel != vguiPanel)
		return;

	// IsButtonDown() Must be called before ResetInputState() or EnableInput()
	Visuals::ChokedPackets();
	Visuals::Run();

	// Draw menu above all other items
	Menu::Draw();
}

static bool __stdcall CreateMove(float input_sample_frametime, CUserCmd* cmd)
{
	auto result = hooks->input->GetOriginal<bool(__thiscall*)(void*, float, CUserCmd*)>(24)(interfaces->ClientMode, input_sample_frametime, cmd);

	if (!cmd || !cmd->commandNumber)
		return result;

	// Whether or not to send the packet
	// false -> packet is choked (maximum of 15 choked packets)
	uintptr_t* frame_pointer;
	__asm mov frame_pointer, ebp;
	bool& send_packet = *reinterpret_cast<bool*>(*frame_pointer - 0x1C);

	QAngle old_angles = cmd->viewangles;
	float old_forward = cmd->forwardmove;
	float old_sidemove = cmd->sidemove;

	Misc::Bhop(cmd);

	// Run packet choking after aimbot/misc stuff
	if (!(cmd->buttons & (CUserCmd::IN_ATTACK | CUserCmd::IN_ATTACK2)))
	{
		Misc::ChokePackets(send_packet);
		AntiAim::Run(cmd, send_packet, old_angles);
	}

	if (Options::rage_aimbot_silent || Options::rage_antiaim_enabled)
		Misc::FixMovement(old_angles, cmd, old_forward, old_sidemove);

	return false;
}

void __fastcall LockCursor(void* ecx)
{
	if (hooks->surface == nullptr)
		return;

	if (!Menu::draw)
		return hooks->surface->GetOriginal<void(__thiscall*)(void*)>(67)(interfaces->Surface);

	interfaces->Surface->UnlockCursor();
	interfaces->InputSystem->ResetInputState();
}

void __fastcall DrawModelExecute(void* _this, int edx, IMatRenderContext* ctx, const DrawModelState_t& state, const ModelRenderInfo_t& pInfo, matrix3x4_t* pCustomBoneToWorld)
{
	static bool once = false;

	if (!once)
	{
		chams = std::make_unique<Chams>();
		once = true;
	}

	if (interfaces->ModelRender->IsForcedMaterialOverride() && !strstr(pInfo.pModel->szName, "arms") && !strstr(pInfo.pModel->szName, "weapons/v_"))
		return hooks->mdl->GetOriginal<void(__thiscall*)(void*, IMatRenderContext*, const DrawModelState_t&, const ModelRenderInfo_t&, matrix3x4_t*)>(21)(_this, ctx, state, pInfo, pCustomBoneToWorld);

	chams->OnDrawModelExecute(ctx, state, pInfo, pCustomBoneToWorld);

	hooks->mdl->GetOriginal<void(__thiscall*)(void*, IMatRenderContext*, const DrawModelState_t&, const ModelRenderInfo_t&, matrix3x4_t*)>(21)(_this, ctx, state, pInfo, pCustomBoneToWorld);

	interfaces->ModelRender->ForcedMaterialOverride(nullptr);
}

static DWORD WINAPI Unload(HMODULE module)
{
	interfaces->~Interfaces();
	chams->~Chams();

	// Wait for hooks to finish their functions
	std::this_thread::sleep_for(std::chrono::milliseconds(50));

	Utilities::DetachConsole();
	FreeLibraryAndExitThread(module, 0);

	return NULL;
}

Hooks::Hooks(HMODULE module)
{
	Utilities::AttachConsole();

	interfaces = std::make_unique<Interfaces>();
	memory = std::make_unique<Memory>();
	netvars = std::make_unique<Netvars>();

	Fonts::Initialize();
	Menu::Initialize();

	this->module = module;

	std::this_thread::sleep_for(std::chrono::milliseconds(25));

	Install();
}

void Hooks::Install()
{
	vgui = new VmtHook(interfaces->Panel);
	vgui->SwapPointer(41, reinterpret_cast<void*>(PaintTraverse));
	vgui->ApplyNewTable();

	surface = new VmtHook(interfaces->Surface);
	surface->SwapPointer(67, reinterpret_cast<void*>(LockCursor));
	surface->ApplyNewTable();

	input = new VmtHook(interfaces->ClientMode);
	input->SwapPointer(24, reinterpret_cast<void*>(CreateMove));
	input->ApplyNewTable();

	mdl = new VmtHook(interfaces->ModelRender);
	mdl->SwapPointer(21, reinterpret_cast<void*>(DrawModelExecute));
	mdl->ApplyNewTable();

	window = FindWindowW(L"Valve001", nullptr);
	original_wnd_proc = WNDPROC(SetWindowLongPtr(window, GWLP_WNDPROC, LONG_PTR(WndProc)));
}

void Hooks::Restore()
{
	vgui->RestoreOldTable();
	surface->RestoreOldTable();
	input->RestoreOldTable();
	mdl->RestoreOldTable();

	SetWindowLongPtr(window, GWLP_WNDPROC, LONG_PTR(original_wnd_proc));

	if (HANDLE thread = CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)Unload, module, NULL, NULL))
		CloseHandle(thread);
}