#pragma once
#include <iostream>

class CGlobalVars;
class CClientState;
class KeyValues;

class Memory
{
public:
	Memory();
	CGlobalVars* GlobalVars;
	CClientState* ClientState;
	uintptr_t KeyValuesFromString;
	KeyValues* (__thiscall* KeyValuesFindKey)(KeyValues* key_values, const char* key_name, bool create);
	void(__thiscall* KeyValuesSetString)(KeyValues* key_values, const char* value);
};

inline std::unique_ptr<Memory> memory;