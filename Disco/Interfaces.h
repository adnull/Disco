#pragma once
#include <iostream>
#include <vector>
#include "PE/Module.h"

class IBaseClientDLL;
class IVPanel;
class ISurface;
class IInputSystem;
class CClientMode;
class IClientEntityList;
class IVEngineClient;
class IVModelRender;
class IMaterialSystem;
class IVDebugOverlay;

class Interface
{
public:
	std::string module_name;
	const char* interface_name;
	uintptr_t create_fn;
};

class Interfaces
{
public:
	Interfaces();
	~Interfaces();
	std::vector<Interface> interfaces;
	IBaseClientDLL* Client;
	IVPanel* Panel;
	ISurface* Surface;
	IInputSystem* InputSystem;
	CClientMode* ClientMode;
	IClientEntityList* ClientEntityList;
	IVEngineClient* Engine;
	IVModelRender* ModelRender;
	IMaterialSystem* MaterialSystem;
	IVDebugOverlay* DebugOverlay;
private:
	Interface FindInterface(std::string interface_name);
};

inline std::unique_ptr<const Interfaces> interfaces;