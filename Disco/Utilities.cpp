#include <iostream>
#include "Utilities.h"
#include "Interfaces.h"
#include "PE/Types.h"
#include "SDK/Entity.h"
#include "SDK/IInputSystem.h"
#include "SDK/IVEngineClient.h"
#include "SDK/IClientEntityList.h"

FILE* f;

void Utilities::AttachConsole()
{
	AllocConsole();
	freopen_s(&f, "CONOUT$", "w", stdout);
}

void Utilities::DetachConsole()
{
	if (f) fclose(f);
	FreeConsole();
}

std::vector<Module> Utilities::ListModules()
{
	// __readfsdword(...) requires x86 build type
	Types::PEB* peb = reinterpret_cast<Types::TEB*>(__readfsdword(0x18))->ProcessEnvironmentBlock;
	uintptr_t base_address = peb->ImageBaseAddress;

	Module module;
	std::vector<Module> modules;

	if (!peb->Ldr->InMemoryOrderModuleList.Flink)
		return modules;

	Types::LIST_ENTRY* list = &peb->Ldr->InMemoryOrderModuleList;

	for (auto i = list->Flink; i != list; i = i->Flink)
	{
		Types::LDR_DATA_TABLE_ENTRY* ldr_entry = CONTAINING_RECORD(i, Types::LDR_DATA_TABLE_ENTRY, InMemoryOrderLinks);

		if (!ldr_entry)
			continue;

		if (!module.InitializeModule(ldr_entry))
			continue;

		modules.push_back(module);
	}

	return modules;
}

uintptr_t Utilities::FollowJump(uintptr_t address)
{
	if (!address)
		return 0;

	uintptr_t displacement = *reinterpret_cast<uintptr_t*>(address);

	if (!displacement)
		return 0;

	return (address + sizeof(uintptr_t)) + displacement;
}

bool Utilities::InterfaceList(HMODULE module, std::vector<InterfaceReg*> &interface_reg_list)
{
	uintptr_t create_interface = (uintptr_t)GetProcAddress(module, "CreateInterface");

	if (!create_interface)
		return false;

	if (*reinterpret_cast<uint8_t*>(create_interface + 4) != 0xE9)
		return false;

	create_interface = FollowJump(create_interface + 5);

	if (!create_interface)
		return false;

	InterfaceReg* interface_list = **reinterpret_cast<InterfaceReg***>(create_interface + 6);

	for (InterfaceReg* current = interface_list; current; current = current->m_pNext)
		interface_reg_list.push_back(current);

	return true;
}

std::string Utilities::LastXOf(std::string input, int number)
{
	return input.substr(input.size() - number);
}

std::string Utilities::GamePath()
{
	TCHAR buffer[MAX_PATH] = { 0 };
	GetModuleFileName(NULL, buffer, MAX_PATH);
	return std::string(buffer);
}

std::wstring Utilities::StdToWString(std::string text)
{
	std::wstring str2(text.length(), L' ');

	// Avoid issues when un-hooking
	if (&str2.begin() == nullptr)
		return str2;

	std::copy(text.begin(), text.end(), str2.begin());

	return str2;
}

bool Utilities::MouseInRegion(int x0, int y0, int x1, int y1)
{
	int x, y;
	interfaces->InputSystem->GetCursorPosition(&x, &y);
	return (x0 < x) && (x < x1) && (y0 < y) && (y < y1);
}

Entity* Utilities::LocalPlayer()
{
	return (Entity*)interfaces->ClientEntityList->GetClientEntity(interfaces->Engine->GetLocalPlayer());
}

std::uint8_t* Utilities::PatternScan(void* module, const char* signature)
{
	static auto pattern_to_bytes = [](const char* pattern) {
		auto bytes = std::vector<int>{};
		auto start = const_cast<char*>(pattern);
		auto end = const_cast<char*>(pattern) + strlen(pattern);

		for (auto current = start; current < end; ++current)
		{
			if (*current == '?')
			{
				++current;

				if (*current == '?')
					++current;

				bytes.push_back(-1);
			}
			else
			{
				bytes.push_back(strtoul(current, &current, 16));
			}
		}

		return bytes;
	};

	auto dos_header = (PIMAGE_DOS_HEADER)module;
	auto nt_headers = (PIMAGE_NT_HEADERS)((std::uint8_t*)module + dos_header->e_lfanew);

	auto size_of_image = nt_headers->OptionalHeader.SizeOfImage;
	auto pattern_bytes = pattern_to_bytes(signature);
	auto scan_bytes = reinterpret_cast<std::uint8_t*>(module);

	auto s = pattern_bytes.size();
	auto d = pattern_bytes.data();

	for (auto i = 0ul; i < size_of_image - s; ++i)
	{
		bool found = true;

		for (auto j = 0ul; j < s; ++j)
		{
			if (scan_bytes[i + j] != d[j] && d[j] != -1)
			{
				found = false;
				break;
			}
		}

		if (found)
		{
			return &scan_bytes[i];
		}
	}

	return nullptr;
}