#include <iostream>
#include <vector>
#include "../SDK/Misc/Color.h"
#include "Option.h"

Option::Option(std::string _name, bool* _parent)
{
	name = _name;
	b_parent = _parent;
	type = OptionTypes::Bool;
}

Option::Option(std::string _name, float* _parent, float _min, float _max)
{
	name = _name;
	f_parent = _parent;
	min = _min;
	max = _max;
	type = OptionTypes::Float;
}

Option::Option(std::string _name, int* _parent, int _min, int _max)
{
	name = _name;
	s_parent = _parent;
	imin = _min;
	imax = _max;
	type = OptionTypes::Int;
}

Option::Option(std::string _name, std::vector<std::string> _strings, int* _parent)
{
	name = _name;
	strings = _strings;
	s_parent = _parent;
	max = _strings.size() - 1;
	type = OptionTypes::Strings;
}

Option::Option(std::string _name, Color* _parent)
{
	name = _name;
	c_parent = _parent;
	type = OptionTypes::Color;
}