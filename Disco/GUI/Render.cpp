#include <iostream>
#include <ctime>
#include <sstream>
#include <algorithm>
#include <sstream>
#include <iomanip>
#include "Render.h"
#include "Menu.h"
#include "Container.h"
#include "../Options.h"
#include "../Utilities.h"
#include "../Interfaces.h"
#include "../SDK/ISurface.h"
#include "../SDK/IInputSystem.h"
#include "../SDK/Misc/Definitions.h"

namespace Fonts
{
	Font Menu;
	Font MenuSmall;
	Font ESP;
}

void Fonts::Initialize()
{
	Menu = interfaces->Surface->CreateFontA();
	MenuSmall = interfaces->Surface->CreateFontA();
	ESP = interfaces->Surface->CreateFontA();

	interfaces->Surface->SetFontGlyphSet(Menu, "Tahoma", 15, 400, 0, 0, FONTFLAG_OUTLINE | FONTFLAG_ANTIALIAS);
	interfaces->Surface->SetFontGlyphSet(MenuSmall, "Tahoma", 12, 400, 0, 0, FONTFLAG_OUTLINE | FONTFLAG_ANTIALIAS);
	interfaces->Surface->SetFontGlyphSet(ESP, "Small Fonts", 14, 400, 0, 0, FONTFLAG_OUTLINE);
}

namespace Render
{
	std::vector<ControlRender> queue;
}

void Render::Window()
{
	static bool menu_dragging = false;
	static int prev_mouse_x, prev_mouse_y, old_offset_x, old_offset_y;

	if (Utilities::MouseInRegion(Menu::offset_x - MENU_DRAG_REGION, Menu::offset_y - MENU_DRAG_REGION, Menu::offset_x + MENU_WIDTH, Menu::offset_y + MENU_DRAG_REGION))
		menu_dragging = true;

	if (menu_dragging)
	{
		int mouse_x, mouse_y;
		interfaces->InputSystem->GetCursorPosition(&mouse_x, &mouse_y);

		if (Menu::clicking)
		{
			Menu::offset_x = (mouse_x - prev_mouse_x) + old_offset_x;
			Menu::offset_y = (mouse_y - prev_mouse_y) + old_offset_y;
		}
		else
		{

			prev_mouse_x = mouse_x;
			prev_mouse_y = mouse_y;

			old_offset_x = Menu::offset_x;
			old_offset_y = Menu::offset_y;

			menu_dragging = false;
		}
	}

	queue.push_back(ControlRender{ Menu::offset_x, Menu::offset_y, MENU_WIDTH, MENU_HEIGHT, RenderType::FilledRect, Options::color_menu_backdrop, "", 0 });
	queue.push_back(ControlRender{ Menu::offset_x + BORDER_WIDTH, Menu::offset_y + BORDER_WIDTH, MENU_WIDTH - (2 * BORDER_WIDTH), MENU_HEIGHT - (2 * BORDER_WIDTH), RenderType::FilledRect, Options::color_menu_background, "", 0 });
	queue.push_back(ControlRender{ Menu::offset_x, Menu::offset_y, MENU_WIDTH, MENU_HEIGHT, RenderType::OutlinedRect, Options::color_menu_outline_out, "", 0 });
	queue.push_back(ControlRender{ Menu::offset_x + BORDER_WIDTH, Menu::offset_y + BORDER_WIDTH, MENU_WIDTH - (2 * BORDER_WIDTH), MENU_HEIGHT - (2 * BORDER_WIDTH), RenderType::OutlinedRect, Options::color_menu_outline_in, "", 0 });
}

void Render::Tabs()
{
	int w, h;
	int tabs_width = 0;

	for (int i = 0; i < Menu::tabs.size(); i++)
	{
		interfaces->Surface->GetTextSize(Fonts::Menu, Utilities::StdToWString(Menu::tabs[i].name).c_str(), w, h);
		tabs_width += w + TAB_SEPARATION;
	}

	// Calculate tab start (centered tabs)
	int tabs_start = (MENU_WIDTH / 2) - (tabs_width / 2);
	int current_pos = 0;

	for (int i = 0; i < Menu::tabs.size(); i++)
	{
		// Get size of the text
		interfaces->Surface->GetTextSize(Fonts::Menu, Utilities::StdToWString(Menu::tabs[i].name).c_str(), w, h);

		// Check if we clicked the tab
		bool in_tab = Utilities::MouseInRegion(Menu::offset_x + tabs_start + current_pos - 1, Menu::offset_y + TAB_SEPARATION, Menu::offset_x + tabs_start + current_pos + w - 1, Menu::offset_y + TAB_SEPARATION + h);

		if (in_tab && Menu::clicking)
		{
			for (int j = 0; j < Menu::tabs.size(); j++)
				Menu::tabs[j].draw = (j == i);

			Menu::combo_target = "";
		}

		Color underline;

		if (Menu::tabs[i].draw)
			underline = Options::color_menu_selected;
		else if (in_tab)
			underline = Options::color_menu_hovered;
		else
			underline = Options::color_menu_background_alt;

		queue.push_back(ControlRender{ Menu::offset_x + tabs_start + current_pos - 1, Menu::offset_y + h + TAB_SEPARATION, w + 1, 0, RenderType::Line, underline, "", 0 });
		queue.push_back(ControlRender{ Menu::offset_x + tabs_start + current_pos - 2, Menu::offset_y + h + TAB_SEPARATION - 1, w + 4, 3, RenderType::OutlinedRect, Options::color_menu_outline_out, "", 0 });
		queue.push_back(ControlRender{ Menu::offset_x + tabs_start + current_pos, Menu::offset_y + TAB_SEPARATION, 0, 0, RenderType::Text, Colors::White, Menu::tabs[i].name, Fonts::Menu });

		current_pos += w + TAB_SEPARATION;
	}
}

void Render::TabContent(Tab tab)
{
	if (!tab.draw)
		return;

	int layer = 0;
	auto containers = tab.containers;

	static int prev_float_x, prev_float_y;

	for (int j = 0; j < containers.size(); j++)
	{
		Render::Container(containers[j].name, layer, !containers[j].half, (j + 1) % 2 == 1);

		auto options = containers[j].options;

		int pos_x = Menu::offset_x + 24;
		int pos_y = Menu::offset_y + 50 + (layer * (CONTAINER_HEIGHT + CONTAINER_SEPARATION));

		bool left_side = ((j + 1) % 2 == 1 && containers[j].half) || !containers[j].half;
		int start_x = left_side ? pos_x : pos_x + (MENU_WIDTH / 2) - 15;
		int current_y = pos_y;

		for (int k = 0; k < options.size(); k++)
		{
			Option option = options[k];

			switch (option.type)
			{
			case (OptionTypes::Bool):
			{
				bool in_region = Utilities::MouseInRegion(start_x, current_y, start_x + 60, current_y + 12);

				if (in_region && Menu::clicking)
				{
					*option.b_parent = !*option.b_parent;
					Menu::clicking = false;
				}

				Render::BoolBox(options[k].name, *options[k].b_parent, in_region, pos_x, current_y, left_side);
				current_y += 15;
				break;
			}
			case (OptionTypes::Float):
			{
				bool in_region = Utilities::MouseInRegion(start_x - 2, current_y + 16, start_x + 132, current_y + 31);

				if (in_region && Menu::clicking)
				{
					int x, y;
					interfaces->InputSystem->GetCursorPosition(&x, &y);

					int w, h;
					interfaces->Surface->GetTextSize(Fonts::MenuSmall, Utilities::StdToWString(std::to_string(*option.f_parent)).c_str(), w, h);

					// Clicking before the current loc. of the float cursor makes the count go down 0.25f
					float new_val = (((float)(x + 2) - (float)start_x) / (((float)start_x + 130.f) - (float)start_x));

					*option.f_parent = std::clamp(new_val * (option.max - option.min), option.min, option.max);
				}

				Render::Float(option.name, *option.f_parent, option.min, option.max, pos_x, current_y, left_side);
				current_y += 40;
				break;
			}
			case (OptionTypes::Int):
			{
				bool in_region = Utilities::MouseInRegion(start_x - 2, current_y + 16, start_x + 132, current_y + 31);

				if (in_region && Menu::clicking)
				{
					int x, y;
					interfaces->InputSystem->GetCursorPosition(&x, &y);

					int w, h;
					interfaces->Surface->GetTextSize(Fonts::MenuSmall, Utilities::StdToWString(std::to_string(*option.s_parent)).c_str(), w, h);

					float new_val = (((float)(x + 2) - (float)start_x) / (((float)start_x + 130.f) - (float)start_x));

					*option.s_parent = std::clamp((int)std::floor(new_val * (option.imax - option.imin)), option.imin, option.imax);
				}

				Render::Float(option.name, *option.s_parent, option.imin, option.imax, pos_x, current_y, left_side, true);
				current_y += 40;
				break;
			}
			case (OptionTypes::Strings):
			{
				bool in_region = Utilities::MouseInRegion(pos_x, current_y + 20, pos_x + 80, current_y + 20 + 17);

				if (in_region && Menu::clicking)
				{
					if (Menu::combo_target == "")
					{
						Menu::combo_target = option.name;
					}
					else if (Menu::combo_target == option.name)
					{
						Menu::combo_target = "";
					}

					Menu::clicking = false;
				}

				bool in_combo_region = Utilities::MouseInRegion(pos_x, current_y + 20 + 17, pos_x + 80, current_y + 20 + 17 + (16 * option.strings.size()));

				if (in_combo_region && Menu::clicking && Menu::combo_target == option.name)
				{
					int x, y;
					interfaces->InputSystem->GetCursorPosition(&x, &y);

					*option.s_parent = std::clamp(((y - (current_y + 17)) / 16) - 1, 0, (int)option.strings.size() - 1);

					Menu::clicking = false;
					Menu::combo_target = "";
				}

				Render::ComboBox(option.name, *option.s_parent, option.strings, Menu::combo_target == option.name, pos_x, current_y);
				current_y += 40;
				break;
			}
			default:
				break;
			}
		}

		// Check if we're on the right
		if (!left_side || !containers[j].half)
			layer += 1;
	}

	if (Menu::clicking && Menu::combo_target != "")
		Menu::combo_target = "";
}

void Render::Container(std::string title, int layer, bool full, bool left)
{
	interfaces->Surface->DrawSetColor(Options::color_menu_selected);

	int container_offset_y = Menu::offset_y + 38 + (layer * (CONTAINER_HEIGHT + CONTAINER_SEPARATION));

	int w, h;
	interfaces->Surface->GetTextSize(Fonts::Menu, Utilities::StdToWString(title).c_str(), w, h);

	if (full)
	{
		queue.push_back(ControlRender{ Menu::offset_x + 15, container_offset_y, MENU_WIDTH - 30, CONTAINER_HEIGHT, RenderType::OutlinedRect, Options::color_menu_selected, "", 0 });
		queue.push_back(ControlRender{ Menu::offset_x + 20, container_offset_y, w + 10, 0, RenderType::Line, Options::color_menu_background, "", 0 });
		queue.push_back(ControlRender{ Menu::offset_x + 25, container_offset_y - (h / 2), 0, 0, RenderType::Text, Colors::White, title, Fonts::Menu });
	}
	else
	{
		int start = 0;

		if (left)
			start =  15;
		else
			start = (MENU_WIDTH / 2) + 5;

		queue.push_back(ControlRender{ Menu::offset_x + start, container_offset_y, (MENU_WIDTH / 2) - 20, CONTAINER_HEIGHT, RenderType::OutlinedRect, Options::color_menu_selected, "", 0 });

		interfaces->Surface->DrawSetColor(Options::color_menu_background);

		if (left)
			start = 20;
		else
			start = 10 + (MENU_WIDTH / 2);

		queue.push_back(ControlRender{ Menu::offset_x + start, container_offset_y, w + 10, 0, RenderType::Line, Options::color_menu_background, "", 0 });
		queue.push_back(ControlRender{ Menu::offset_x + start + 4, container_offset_y - (h / 2), 0, 0, RenderType::Text, Colors::White, title, Fonts::Menu });
	}
}

void Render::BoolBox(std::string title, bool value, bool hovered, int posx, int posy, bool left)
{
	int start_x = (left ? posx : posx + (MENU_WIDTH / 2) - 10) + 1; // Correct outline
	Color color = hovered ? Options::color_menu_background_alt : Options::color_menu_background;

	queue.push_back(ControlRender{ start_x, posy, 10, 10, RenderType::FilledRect, color, "", 0 });
	queue.push_back(ControlRender{ start_x + 1, posy + 1, 10 - 2, 10 - 2, RenderType::FilledRect, value ? Options::color_menu_selected : color, "", 0 });
	queue.push_back(ControlRender{ start_x - 1, posy - 1, 10 + 2, 10 + 2, RenderType::OutlinedRect, Colors::White, "", 0 });
	queue.push_back(ControlRender{ start_x + 18, posy - 3, 0, 0, RenderType::Text, Colors::White, title, Fonts::Menu });
}

void Render::Float(std::string title, float value, float min, float max, int posx, int posy, bool left, bool round)
{
	std::stringstream ss;
	ss << std::fixed << std::setprecision(round ? 0 : 2) << value;
	std::wstring text = Utilities::StdToWString(ss.str());

	int w, h;
	interfaces->Surface->GetTextSize(Fonts::MenuSmall, text.c_str(), w, h);

	int start_x = left ? posx : posx + (MENU_WIDTH / 2) - 10;
	int set_pos_y = posy + 20;

	queue.push_back(ControlRender{ start_x, posy, 0, 0, RenderType::Text, Colors::White, title, Fonts::Menu });
	queue.push_back(ControlRender{ start_x, set_pos_y - 3, 130, 13, RenderType::FilledRect, Options::color_menu_background, "", 0 });
	queue.push_back(ControlRender{ start_x + 2, set_pos_y - 1, (int)(128 * ((float)value / ((float)max - (float)min))) - 2, 9, RenderType::FilledRect, Options::color_menu_selected, "", 0 });
	queue.push_back(ControlRender{ start_x, set_pos_y - 3, 130, 13, RenderType::OutlinedRect, Colors::White, "", 0 });
	queue.push_back(ControlRender{ start_x + ((130 / 2) - (w / 2)), set_pos_y - 3, 0, 0, RenderType::Text, Colors::White, ss.str(), Fonts::MenuSmall });
}

void Render::ComboBox(std::string title, int chosen, std::vector<std::string> strings, bool open, int posx, int posy)
{
	int set_pos_y = posy + 20;

	queue.push_back(ControlRender{ posx, posy, 0, 0, RenderType::Text, Colors::White, title, Fonts::Menu });
	queue.push_back(ControlRender{ posx, set_pos_y, 80, 17, RenderType::OutlinedRect, Colors::White, "", 0 });
	queue.push_back(ControlRender{ posx + 6, set_pos_y + 1, 0, 0, RenderType::Text, Colors::White, strings[chosen], Fonts::Menu });

	if (!open)
		return;

	queue.push_back(ControlRender{ posx, set_pos_y + 16, 80, (int)(16 * strings.size()), RenderType::FilledRect, Options::color_menu_background, "", 0, 1 });
	queue.push_back(ControlRender{ posx, set_pos_y + 16, 80, (int)(16 * strings.size()) + 1, RenderType::OutlinedRect, Colors::White, "", 0, 1 });

	for (int i = 0; i < strings.size(); i++)
	{
		if (chosen == i)
			queue.push_back(ControlRender{ posx, set_pos_y + 17 + (16 * i), 80, 15, RenderType::FilledRect, Options::color_menu_background_alt, "", 0, 1 });
		queue.push_back(ControlRender{ posx + 6, set_pos_y + 17 + (16 * i), 0, 0, RenderType::Text, Colors::White, strings[i], Fonts::Menu, 1 });
	}
}

void Render::Watermark()
{
	tm new_time;
	std::time_t time = std::time(0);
	localtime_s(&new_time, &time);

	char watermark_text[14];
	sprintf_s(watermark_text, "Disco | %02d:%02d", new_time.tm_hour, new_time.tm_min);

	queue.push_back(ControlRender{ 10, 10, 88, 25, RenderType::FilledRect, Options::color_menu_backdrop, "", 0 });
	queue.push_back(ControlRender{ 12, 12, 84, 21, RenderType::FilledRect, Options::color_menu_background, "", 0 });
	queue.push_back(ControlRender{ 10, 10, 88, 25, RenderType::OutlinedRect, Options::color_menu_outline_out, "", 0 });
	queue.push_back(ControlRender{ 12, 12, 84, 21, RenderType::OutlinedRect, Options::color_menu_selected, "", 0 });
	queue.push_back(ControlRender{ 18, 15, 0, 0, RenderType::Text, Colors::White, watermark_text, Fonts::Menu });
}

void DoRender(ControlRender item)
{
	switch (item.type)
	{
	case (RenderType::Line):
	{
		interfaces->Surface->DrawSetColor(item.color);
		interfaces->Surface->DrawLine(item.x, item.y, item.x + item.size_x, item.y + item.size_y);
		break;
	}
	case (RenderType::OutlinedRect):
	{
		interfaces->Surface->DrawSetColor(item.color);
		interfaces->Surface->DrawOutlinedRect(item.x, item.y, item.x + item.size_x, item.y + item.size_y);
		break;
	}
	case (RenderType::FilledRect):
	{
		interfaces->Surface->DrawSetColor(item.color);
		interfaces->Surface->DrawFilledRect(item.x, item.y, item.x + item.size_x, item.y + item.size_y);
		break;
	}
	case (RenderType::Text):
	{
		interfaces->Surface->DrawSetTextPos(item.x, item.y);
		interfaces->Surface->DrawSetTextColor(item.color);
		interfaces->Surface->DrawSetTextFont(item.font);
		interfaces->Surface->DrawPrintText(Utilities::StdToWString(item.text).c_str(), item.text.length());
		break;
	}
	default:
		break;
	}
}

void Render::Queue(std::vector<ControlRender> &items)
{
	std::vector<ControlRender> higher_level;

	for (ControlRender item : items)
	{
		if (item.layer == 1)
		{
			item.layer = 0;
			higher_level.push_back(item);
			continue;
		}

		DoRender(item);
	}

	for (ControlRender item : higher_level)
		DoRender(item);

	items.clear();
}