#pragma once
#include <iostream>
#include <vector>
#include "../SDK/Misc/Definitions.h"
#include "../SDK/Misc/Color.h"

class Tab;

enum class RenderType
{
	Line = 0,
	OutlinedRect,
	FilledRect,
	Text
};

struct ControlRender
{
	int x;
	int y;
	int size_x;
	int size_y;
	RenderType type;
	Color color;
	std::string text;
	unsigned long font;
	int layer = 0;
};

namespace Fonts
{
	extern Font Menu;
	extern Font MenuSmall;
	extern Font ESP;
	void Initialize();
}

namespace Render
{
	void Window();
	void Tabs();
	void TabContent(Tab tab);
	void Container(std::string title, int layer, bool full, bool left = false);
	void BoolBox(std::string title, bool value, bool hovered, int posx, int posy, bool left = false);
	void Float(std::string title, float value, float min, float max, int posx, int posy, bool left = false, bool round = false);
	void ComboBox(std::string title, int chosen, std::vector<std::string> strings, bool open, int posx, int posy);
	void Watermark();
	void Queue(std::vector<ControlRender>& items);
	extern std::vector<ControlRender> queue;
};