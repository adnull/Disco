#pragma once
#include <iostream>
#include <vector>
#include "Option.h"

class Container;

class Tab
{
public:
	Tab(std::string _name);
	void AddOption(Option option);
	void AddContainer(Container tab);
	void Draw();
	std::string name;
	std::vector<Option> options;
	std::vector<Container> containers;
	bool draw = false;
};