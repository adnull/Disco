#pragma once

class Color;

enum class OptionTypes
{
	Bool,
	Float,
	Int,
	Strings,
	Color
};

class Option
{
public:
	std::string name;
	OptionTypes type;
	bool* b_parent;
	float* f_parent;
	int* s_parent;
	Color* c_parent;
	std::vector<std::string> strings;
	int s_max;
	float min;
	float max;
	int imin;
	int imax;
	Option();
	Option(std::string _name, bool* _parent);
	Option(std::string _name, float* _parent, float _min, float _max);
	Option(std::string _name, int* _parent, int _min, int _max);
	Option(std::string _name, std::vector<std::string> _strings, int* _parent);
	Option(std::string _name, Color* _parent);
};