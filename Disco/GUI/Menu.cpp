#include "Render.h"
#include "Menu.h"
#include "Container.h"
#include "../Utilities.h"
#include "../Interfaces.h"
#include "../Options.h"
#include "../SDK/ISurface.h"
#include "../SDK/IInputSystem.h"
#include "../SDK/Misc/Color.h"

namespace Menu
{
	int offset_x = 40;
	int offset_y = 40;
	bool draw = false;
	bool clicking = true;
	std::string f_drag_target;
	std::vector<Tab> tabs;
	std::string combo_target = "";
}

void Menu::Initialize()
{
	Tab legit = Tab("Legit");

	// Legit aimbot tab
	Container c_legit_aimbot = Container("Aimbot");
	c_legit_aimbot.AddOption(Option("Enabled", &Options::legit_aimbot_enabled));
	c_legit_aimbot.AddOption(Option("Smooth", &Options::legit_aimbot_smooth, 0.0f, 10.0f));

	// Legit triggerbot tab
	Container c_legit_trigger = Container("Triggerbot");
	legit.AddContainer(c_legit_aimbot);
	legit.AddContainer(c_legit_trigger);

	Tab rage = Tab("Rage");
	Container c_rage_aimbot = Container("Aimbot");
	Container c_rage_antiaim = Container("Anti Aim");

	c_rage_antiaim.AddOption(Option("Enabled", &Options::rage_antiaim_enabled));
	c_rage_antiaim.AddOption(Option("Real Yaw", std::vector<std::string>{"None", "Backwards", "Static", "Spin", "Jitter"}, & Options::rage_antiaim_real_yaw));
	c_rage_antiaim.AddOption(Option("Real Pitch", std::vector<std::string>{"None", "Down", "Up"}, & Options::rage_antiaim_real_pitch));
	c_rage_antiaim.AddOption(Option("Fake (desync)", std::vector<std::string>{"None", "On (WTF)"}, & Options::rage_antiaim_fake_yaw));
	c_rage_antiaim.AddOption(Option("Choked Packets", &Options::rage_choked_packets, 0, 15));

	rage.AddContainer(c_rage_aimbot);
	rage.AddContainer(c_rage_antiaim);

	Tab visuals = Tab("Visuals");

	// ESP box tab
	Container c_visuals_esp = Container("ESP");
	c_visuals_esp.AddOption(Option("Box", &Options::visuals_esp_box));
	c_visuals_esp.AddOption(Option("Outlined", &Options::visuals_esp_outlined));
	c_visuals_esp.AddOption(Option("Health", &Options::visuals_esp_health));
	c_visuals_esp.AddOption(Option("Armour", &Options::visuals_esp_armour));
	c_visuals_esp.AddOption(Option("Name", &Options::visuals_esp_name));
	c_visuals_esp.AddOption(Option("Distance", &Options::visuals_esp_distance));
	c_visuals_esp.AddOption(Option("Fill", &Options::visuals_esp_fill));
	c_visuals_esp.AddOption(Option("Enemy Only", &Options::visuals_esp_enemy_only));
	c_visuals_esp.AddOption(Option("On Death Only", &Options::visuals_esp_on_death));
	c_visuals_esp.AddOption(Option("Snaplines", &Options::visuals_esp_snaplines));
	c_visuals_esp.half = true;

	Container c_visuals_glow = Container("Glow");
	c_visuals_glow.AddOption(Option("Enabled", &Options::visuals_glow_enabled));
	c_visuals_glow.half = true;

	Container c_visuals_chams = Container("Chams");
	c_visuals_chams.AddOption(Option("Enabled", &Options::visuals_chams_enabled));
	c_visuals_chams.AddOption(Option("Hands", std::vector<std::string> {"None", "Invisible", "Regular", "Flat", "Wireframe", "Glass"}, &Options::visuals_chams_hand_type));
	c_visuals_chams.AddOption(Option("Weapon", std::vector<std::string> {"None", "Invisible", "Regular", "Flat", "Wireframe", "Glass"}, & Options::visuals_chams_weapon_type));
	c_visuals_chams.AddOption(Option("Players", std::vector<std::string> {"None", "Regular", "Flat", "Wireframe", "Glass"}, & Options::visuals_chams_players_type));

	visuals.AddContainer(c_visuals_esp);
	visuals.AddContainer(c_visuals_glow);
	visuals.AddContainer(c_visuals_chams);

	Tab misc = Tab("Misc");

	Container c_misc_movement = Container("Movement");
	c_misc_movement.AddOption(Option("Bhop", &Options::misc_movement_bhop));
	c_misc_movement.AddOption(Option("Autostrafe", &Options::misc_movement_autostrafe));

	Container c_misc_cheat = Container("Cheat");
	c_misc_cheat.AddOption(Option("Animated Clantag", &Options::misc_cheat_animated_clantag));

	misc.AddContainer(c_misc_movement);
	misc.AddContainer(c_misc_cheat);

	Tab config = Tab("Config");

	legit.draw = true;

	tabs.push_back(legit);
	tabs.push_back(rage);
	tabs.push_back(visuals);
	tabs.push_back(misc);
	tabs.push_back(config);
}

void Menu::Draw()
{
	if (draw)
	{
		Render::Window();
		Render::Tabs();

		for (auto tab : Menu::tabs)
		{
			if (tab.draw)
				Render::TabContent(tab);
		}
	}

	Render::Watermark();
	Render::Queue(Render::queue);
}

void Menu::Controls(UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_LBUTTONDOWN:
		clicking = true;
		break;
	case WM_LBUTTONUP:
		clicking = false;
		break;
	default:
		break;
	}
}