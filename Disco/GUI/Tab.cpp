#include "Tab.h"
#include "Option.h"
#include "Container.h"

Tab::Tab(std::string _name)
{
	name = _name;
}

void Tab::AddOption(Option option)
{
	options.push_back(option);
}

void Tab::AddContainer(Container container)
{
	containers.push_back(container);
}

void Tab::Draw()
{

}