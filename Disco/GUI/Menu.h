#pragma once
#include "Tab.h"
#include <Windows.h>

namespace Menu
{
	void Initialize();
	void Controls(UINT msg, WPARAM wParam, LPARAM lParam);
	void Draw();
	extern int offset_x;
	extern int offset_y;
	extern bool draw;
	extern bool clicking;
	extern std::string f_drag_target;
	extern std::vector<Tab> tabs;
	extern std::string combo_target;
}

#define MENU_WIDTH 300
#define MENU_HEIGHT 470
#define MENU_DRAG_REGION 10
#define BORDER_WIDTH 5
#define TAB_SEPARATION 10
#define CONTAINER_HEIGHT 200
#define CONTAINER_SEPARATION 10