#pragma once
#include <iostream>
#include <vector>

class Option;

class Container
{
public:
	Container(std::string _name);
	void AddOption(Option _option);
	std::string name;
	std::vector<Option> options;
	bool half = false;
};