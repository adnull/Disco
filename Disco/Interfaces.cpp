#include "Interfaces.h"
#include "Utilities.h"
#include "SDK/IInputSystem.h"
#include "SDK/IMaterialSystem.h"
#include "SDK/Misc/InterfaceReg.h"
#include <fstream>
#include <iostream>

Interfaces::Interfaces()
{
	std::vector<InterfaceReg*> interface_reg_list;

	HMODULE handle;
	InterfaceReg* current;

	for (Module module : Utilities::ListModules())
	{
		handle = GetModuleHandleW(module.GetModuleNameW().c_str());

		if (!handle)
			continue;

		if (!Utilities::InterfaceList(handle, interface_reg_list))
			continue;

		for (InterfaceReg* current : interface_reg_list)
			interfaces.push_back(Interface{ module.GetModuleNameA(), current->m_pName, reinterpret_cast<uintptr_t>(current->m_CreateFn()) });
	}

	Client = reinterpret_cast<IBaseClientDLL*>(FindInterface("VClient0").create_fn);
	Panel = reinterpret_cast<IVPanel*>(FindInterface("VGUI_Panel0").create_fn);
	Surface = reinterpret_cast<ISurface*>(FindInterface("VGUI_Surface0").create_fn);
	InputSystem = reinterpret_cast<IInputSystem*>(FindInterface("InputSystemVersion0").create_fn);
	ClientMode = **reinterpret_cast<CClientMode***>((*reinterpret_cast<uintptr_t**>(Client))[10] + 5);
	ClientEntityList = reinterpret_cast<IClientEntityList*>(FindInterface("VClientEntityList0").create_fn);
	Engine = reinterpret_cast<IVEngineClient*>(FindInterface("VEngineClient0").create_fn);
	ModelRender = reinterpret_cast<IVModelRender*>(FindInterface("VEngineModel0").create_fn);
	MaterialSystem = reinterpret_cast<IMaterialSystem*>(FindInterface("VMaterialSystem0").create_fn);
	DebugOverlay = reinterpret_cast<IVDebugOverlay*>(FindInterface("VDebugOverlay0").create_fn);
}

Interfaces::~Interfaces()
{
	InputSystem->EnableInput(true);
}

Interface Interfaces::FindInterface(std::string interface_name)
{
	Interface current, target;

	for (Interface current : interfaces)
	{
		if (strncmp(current.interface_name, interface_name.c_str(), interface_name.size()))
			continue;

		target = current;
		break;
	}

	return target;
}