#include "Options.h"
#include "SDK/Misc/Color.h"

namespace Options
{
	int rage_choked_packets = 0;
	bool rage_antiaim_enabled = false;
	int rage_antiaim_real_yaw = 0;
	int rage_antiaim_real_pitch = 0;
	int rage_antiaim_fake_yaw = 0;
	bool rage_aimbot_silent = false;

	bool legit_aimbot_enabled = false;
	float legit_aimbot_smooth = 5.0f;

	bool visuals_esp_box = false;
	bool visuals_esp_outlined = false;
	bool visuals_esp_health = false;
	bool visuals_esp_armour = false;
	bool visuals_esp_name = false;
	bool visuals_esp_distance = false;
	bool visuals_esp_fill = false;
	bool visuals_esp_enemy_only = false;
	bool visuals_esp_on_death = false;
	bool visuals_esp_snaplines = false;

	bool visuals_glow_enabled = false;

	bool visuals_chams_enabled = false;
	int visuals_chams_hand_type = 0;
	int visuals_chams_weapon_type = 0;
	int visuals_chams_players_type = 0;

	bool misc_movement_bhop = false;
	bool misc_movement_autostrafe = false;

	bool misc_cheat_animated_clantag = false;

	Color color_menu_background = Color(31, 31, 31, 255);
	Color color_menu_background_alt = Color(128, 128, 128, 255);
	Color color_menu_backdrop = Color(31, 31, 31, 128);
	Color color_menu_outline_out = Color(0, 0, 0, 255);
	Color color_menu_outline_in = Color(92, 187, 237, 255);
	Color color_menu_hovered = Color(220, 220, 220, 255);
	Color color_menu_selected = Color(92, 187, 237, 255);
}