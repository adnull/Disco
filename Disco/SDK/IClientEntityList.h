#pragma once
#include "Entity.h"
#include "../VirtualFunc.h"

class IClientEntityList
{
public:
	Entity* GetClientEntity(int entnum)
	{
		typedef Entity* (__thiscall* oGetEntity)(void*, int);
		return GetVFunc<oGetEntity>(this, 3)(this, entnum);
	}
	int GetHighestEntityIndex()
	{
		typedef int(__thiscall* oGetHighestEntityIndex)(void*);
		return GetVFunc<oGetHighestEntityIndex>(this, 6)(this);
	}
};