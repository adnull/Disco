#pragma once
#include "../VirtualFunc.h"

typedef struct player_info_s
{
    char			pad_0x00[0x10];
    char            name[128];
    int             userID;
    char            guid[33];
    unsigned long   friendsID;
    char            friendsName[128];
    bool            fakeplayer;
    bool            ishltv;
    unsigned int    customFiles[4];
    unsigned char   filesDownloaded;
    char pad_big[0x200];
} player_info_t;

class IVEngineClient
{
public:
	bool GetPlayerInfo(int ent_num, player_info_t* pinfo)
	{
		typedef bool(__thiscall* oGetPlayerInfo)(void*, int, player_info_t*);
		return GetVFunc<oGetPlayerInfo>(this, 8)(this, ent_num, pinfo);
	}
	int GetLocalPlayer()
	{
		typedef int(__thiscall* OriginalFn)(void*);
		return GetVFunc<OriginalFn>(this, 12)(this);
	}
	int GetMaxClients()
	{
		typedef int(__thiscall* oGetMaxClients)(void*);
		return GetVFunc<oGetMaxClients>(this, 20)(this);
	}
	bool IsInGame()
	{
		typedef bool(__thiscall* OriginalFn)(void*);
		return GetVFunc<OriginalFn>(this, 26)(this);
	}
};