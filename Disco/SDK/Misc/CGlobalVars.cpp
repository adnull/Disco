#include "CGlobalVars.h"
#include "CUserCmd.h"
#include "../Entity.h"
#include "../../Utilities.h"

float CGlobalVars::ServerTime(CUserCmd* cmd) noexcept
{
	static int tick;
	static CUserCmd* lastCmd;
	Entity* local_player = Utilities::LocalPlayer();

	if (cmd)
	{
		if (local_player && (!lastCmd || lastCmd->hasbeenpredicted))
			tick = local_player->TickBase();
		else
			tick++;

		lastCmd = cmd;
	}

	return tick * intervalPerTick;
}