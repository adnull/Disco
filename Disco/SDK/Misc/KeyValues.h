#pragma once

class KeyValues
{
public:
	static KeyValues* FromString(const char* name, const char* value);
	KeyValues* FindKey(const char* key_name, bool create);
	void SetString(const char* key_name, const char* value);
};