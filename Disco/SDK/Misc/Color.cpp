#include "Color.h"

namespace Colors
{
	Color Red = Color(255, 0, 0, 255);
	Color Green = Color(0, 255, 0, 255);
	Color Blue = Color(0, 0, 255, 255);
	Color Yellow = Color(255, 255, 0, 255);
	Color White = Color(255, 255, 255, 255);
	Color Black = Color(0, 0, 0, 255);
	Color Grey = Color(100, 100, 100, 255);
	Color PastelBlue = Color(125, 214, 232, 255);
}