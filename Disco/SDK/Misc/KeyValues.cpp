#include "KeyValues.h"
#include "../../Memory.h"

KeyValues* KeyValues::FromString(const char* name, const char* value)
{
	const auto key_values_from_string = memory->KeyValuesFromString;
	KeyValues* key_values;

	__asm {
		push 0
		mov edx, value
		mov ecx, name
		call key_values_from_string
		add esp, 4
		mov key_values, eax
	}

	return key_values;
}

KeyValues* KeyValues::FindKey(const char* key_name, bool create)
{
	return memory->KeyValuesFindKey(this, key_name, create);
}

void KeyValues::SetString(const char* key_name, const char* value)
{
	if (const auto key = FindKey(key_name, true))
		memory->KeyValuesSetString(key, value);
}