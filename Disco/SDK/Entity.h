#pragma once
#include "../Netvars.h"
#include "Misc/CAnimState.h"
#include "Misc/Definitions.h"
#include "IClientNetworkable.h"
#include "IClientEntity.h"
#include "IVEngineClient.h"
#include "../Interfaces.h"
#include <algorithm>

class ICollideable;

class Entity : public IClientEntity
{
public:
	int Health()
	{
		return *reinterpret_cast<int*>(reinterpret_cast<uintptr_t>(this) + netvars->m_iHealth);
	}
	int Armour()
	{
		return *(int*)((uintptr_t)this + netvars->m_ArmorValue);
	}
	int Team()
	{
		return *(int*)((uintptr_t)this + netvars->m_iTeamNum);
	}
	int Flags()
	{
		return *(int*)((uintptr_t)this + netvars->m_fFlags);
	}
	int TickBase()
	{
		return *(int*)((uintptr_t)this + netvars->m_nTickBase);
	}
	CAnimState* GetAnimState()
	{
		return *reinterpret_cast<CAnimState**>(this + 0x3914); // dont hardcode?
	}
	float MaxDesyncAngle()
	{
		const CAnimState* anim_state = GetAnimState();

		if (!anim_state)
			return 0.f;

		float yaw_mod = (anim_state->stopToFullRunningFraction * -0.3f - 0.2f) * std::clamp(anim_state->footSpeed, 0.0f, 1.0f) + 1.0f;

		if (anim_state->duckAmount > 0.0f)
		{
			yaw_mod += (anim_state->duckAmount * std::clamp(anim_state->footSpeed2, 0.0f, 1.0f) * (0.5f - yaw_mod));
		}

		return anim_state->velocitySubtractY * yaw_mod;
	}
	Vector VecOrigin()
	{
		return *(Vector*)((uintptr_t)this + netvars->m_vecOrigin);
	}
	Vector ViewOffset()
	{
		return *(Vector*)((uintptr_t)this + netvars->m_vecViewOffset);
	}
	Vector HeadPosition()
	{
		return VecOrigin() + ViewOffset();
	}
	ICollideable* GetCollideable()
	{
		return (ICollideable*)((uintptr_t)this + netvars->m_Collision);
	}
	player_info_t GetPlayerInfo()
	{
		player_info_t info;
		interfaces->Engine->GetPlayerInfo(((IClientNetworkable*)this)->GetIndex(), &info);

		return info;
	}
	int GetDistance(Entity* target)
	{
		Vector our_pos = this->VecOrigin();
		Vector other_pos = target->VecOrigin();

		return sqrt(
			(pow(other_pos.x - our_pos.x, 2)) +
			(pow(other_pos.y - our_pos.y, 2)) +
			(pow(other_pos.z - our_pos.z, 2))
		);
	}
	float BombTimer()
	{
		return *(float*)((uintptr_t)this + netvars->m_flC4Blow);
	}
	float BombTimerLength()
	{
		return *(float*)((uintptr_t)this + netvars->m_flTimerLength);
	}
	bool IsC4()
	{
		// don't cast, inherit n fix l8r
		IClientEntity* entity = reinterpret_cast<IClientEntity*>(this);
		return entity->GetClientClass()->m_ClassID == ClassId_CPlantedC4;
	}
	LifeState GetLifeState()
	{
		return *(LifeState*)((uintptr_t)this + netvars->m_lifeState);
	}
	bool IsAlive()
	{
		return this->Health() > 0 && this->GetLifeState() == LIFE_ALIVE;
	}
};