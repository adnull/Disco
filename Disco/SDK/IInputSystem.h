#pragma once
#include "IAppSystem.h"
#include "../VirtualFunc.h"

class IInputSystem
{
public:
	void EnableInput(bool bEnable)
	{
		typedef void(__thiscall* oEnableInput)(void*, bool);
		return GetVFunc<oEnableInput>(this, 11)(this, bEnable);
	}
	bool IsButtonDown(ButtonCode_t code)
	{
		typedef bool(__thiscall* oIsButtonDown)(void*, ButtonCode_t);
		return GetVFunc<oIsButtonDown>(this, 15)(this, code);
	}
	void ResetInputState()
	{
		typedef void(__thiscall* oResetInputState)(void*);
		return GetVFunc<oResetInputState>(this, 39)(this);
	}
	void GetCursorPosition(int* m_pX, int* m_pY)
	{
		typedef void(__thiscall* oGetCursorPosition)(void*, int*, int*);
		return GetVFunc<oGetCursorPosition>(this, 56)(this, m_pX, m_pY);
	}
};