#pragma once
#include <cmath>
#include "Vector.h"
#include "QAngle.h"

#define PI 3.14159265359

// LNK2005 -> create .cpp file and create forward decs
double DEG2RAD(double degrees);
void SinCos(float radians, float* sine, float* cosine);
void AngleVectors(const QAngle& angles, Vector* forward, Vector* right, Vector* up);
void AWAngleVectors(const Vector& angles, Vector& forward);
void VectorAngles(const Vector& forward, Vector& angles);