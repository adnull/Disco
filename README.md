# Disco
My Counter-Strike: Global Offensive cheat. Used to be made for any Source game, however I don't think I am ready for a project of that size yet.

Some features, such as aimbot or anti-aim, may be janky/not work at all.

## Features
* Automatic netvar and interface gathering using PE headers
* Clickable/movable menu

## Building
Open the project in Visual Studio 2019 and build as "Release - x86". Use the following values for the general properties.

![](https://puu.sh/HmpgB/64b511ba78.png)